# build


## itk-daq-sw build

Build of `itk-daq-sw` for FELIX-Star, includes all `*.so` libraries.

Compiled with `felix-distribution` https://gitlab.cern.ch/atlas-tdaq-felix/felix-distribution/

